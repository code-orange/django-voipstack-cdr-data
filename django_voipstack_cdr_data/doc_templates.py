from textwrap import wrap

from django.utils import timezone
from django.utils.formats import date_format
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen.canvas import ImageReader
from reportlab.platypus import SimpleDocTemplate
from django.utils.translation import gettext_lazy as _


class CdrReportDocTemplate(SimpleDocTemplate):
    def __init__(self, filename, subtitle: str):
        super().__init__(
            filename=filename,
            pagesize=A4,
            bottomMargin=25,
            rightMargin=10,
            leftMargin=10,
        )
        # Split subtitle into several lines to avoid clipping
        self.subtitle_lines = wrap(subtitle, 38)

        # Set the top margin of the content depending on the number of lines of the subtitle
        self.topMargin = 85 + (len(self.subtitle_lines) - 1) * 23

    def create_header_and_footer(self, canvas, doc):
        # Header
        logo_path = (
            "django_customization_dolphinit/django_customization_dolphinit/static/company/logo"
            "/logo_quadrat_pos_4c_rgb.png"
        )

        image = ImageReader(logo_path)
        w, h = image.getSize()
        aspect_ratio = w / h

        img_width = 128
        img_height = img_width / aspect_ratio

        canvas.drawImage(
            image,
            0,
            self.pagesize[1] - img_height,
            width=img_width,
            height=img_height,
            mask="auto",
        )

        canvas.setFontSize(18)
        canvas.drawCentredString(
            self.pagesize[0] / 2, self.pagesize[1] - 33, str(_("CDR-Report"))
        )

        for i, line in enumerate(self.subtitle_lines):
            canvas.drawCentredString(
                self.pagesize[0] / 2,
                self.pagesize[1] - 56 - i * 23,
                line,
            )

        canvas.setFontSize(11)
        canvas.drawRightString(
            self.pagesize[0] - 15,
            self.pagesize[1] - 26,
            str(
                date_format(
                    timezone.now().date(),
                    use_l10n=True,
                    format="SHORT_DATE_FORMAT",
                )
            ),
        )

        # Footer
        page_number = canvas.getPageNumber()
        canvas.drawRightString(self.pagesize[0] - 15, 15, f"{page_number}")
        canvas.setFontSize(8)
        canvas.drawString(
            15,
            15,
            "Dolphin IT-Systeme e.K., Clausewitzstr. 47A, 42389 Wuppertal, "
            + _("Germany")
            + ", Tel.: +49 202 430401-0, "
            + "E-Mail: hotline@dolphin-it.de, "
            + "USt. - ID: DE228675548",
        )

    def build(self, flowables):
        super().build(
            flowables,
            self.create_header_and_footer,
            self.create_header_and_footer,
        )
