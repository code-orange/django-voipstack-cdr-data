import csv
from datetime import timedelta
from decimal import Decimal
from io import BytesIO, StringIO
from math import ceil

from django.core.cache import cache
from django.db.models.functions import Length
from django.utils.translation import gettext_lazy as _
from phonenumbers import PhoneNumberType, PhoneNumber
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import cm
from reportlab.platypus import Table, Paragraph

from django_mdat_phone_billing.django_mdat_phone_billing.models import *
from django_voipstack_cdr_data.django_voipstack_cdr_data.doc_templates import (
    CdrReportDocTemplate,
)
from django_voipstack_cdr_data.django_voipstack_cdr_data.models import *
from django_voipstack_cdr_data.django_voipstack_cdr_data.templates.telephone_special_numbers.csv_helper import (
    get_special_numbers,
)
from django_voipstack_numfilter.django_voipstack_numfilter.models import *


def get_zone(phone_number: PhoneNumber):
    phone_number_as_int = int(
        phonenumbers.format_number(
            phone_number, phonenumbers.PhoneNumberFormat.E164
        ).replace("+", "")
    )

    # check special routing - 1st step - exact match
    special_routes = MdatPhoneBillingRouteItems.objects.filter(
        routing_key=phone_number_as_int
    ).filter(ident_type=100)

    if len(special_routes) > 0:
        return special_routes.first().item

    # check special routing - 2nd step - route match
    special_routes_ident_type_200 = cache.get("special_routes_ident_type_200")

    if not special_routes_ident_type_200:
        special_routes_ident_type_200 = list(
            MdatPhoneBillingRouteItems.objects.filter(ident_type=200).order_by(
                Length("routing_key").desc()
            )
        )
        cache.set("special_routes_ident_type_200", special_routes_ident_type_200, 3600)

    for special_route_ident_type_200 in special_routes_ident_type_200:
        if str(phone_number_as_int).startswith(
            str(special_route_ident_type_200.routing_key)
        ):
            return special_route_ident_type_200.item

    try:
        phone_country = MdatPhoneBillingCountryItems.objects.get(
            country_code__country_code=phone_number.country_code,
        )
    except MdatPhoneBillingCountryItems.DoesNotExist:
        phone_country = MdatPhoneBillingCountryItems.objects.get(
            country_code__country_code=49,
        )
    except MdatPhoneBillingCountryItems.MultipleObjectsReturned:
        phone_country = MdatPhoneBillingCountryItems.objects.filter(
            country_code__country_code=phone_number.country_code,
        )[0]

    # Check if landline or mobile
    phone_number_type = phonenumbers.number_type(phone_number)

    if phone_number_type == PhoneNumberType.MOBILE:
        return phone_country.item_mobile
    elif phone_number_type == PhoneNumberType.FIXED_LINE_OR_MOBILE:
        return phone_country.item_mobile
    elif phone_number_type == PhoneNumberType.VOIP:
        return phone_country.item_mobile
    elif phone_number_type == PhoneNumberType.PAGER:
        return phone_country.item_mobile
    elif phone_number_type == PhoneNumberType.VOICEMAIL:
        return phone_country.item_mobile
    elif phone_number_type == PhoneNumberType.PERSONAL_NUMBER:
        return phone_country.item_mobile
    elif phone_number_type == PhoneNumberType.TOLL_FREE:
        return phone_country.item_toll_free
    else:
        return phone_country.item_fixed_line


def is_special_number(phone_number: PhoneNumber):
    phone_number_as_int = int(
        phonenumbers.format_number(
            phone_number, phonenumbers.PhoneNumberFormat.E164
        ).replace("+", "")
    )

    # check special routing
    special_numbers = get_special_numbers("de")

    for special_number in special_numbers:
        if str(phone_number_as_int).startswith(str(special_number)):
            return True

        return False


def generate_cdr_data_report(trunk: VoipTrunks, date_from: datetime, date_to: datetime):
    last_full_sync = (
        VoipCdrSyncState.objects.all()
        .values("sync_state_date")
        .order_by("sync_state_date")
        .first()["sync_state_date"]
    )

    # report must be old to new
    if date_to <= date_from:
        return False

    # force all data to be complete before report is generated
    if date_to > last_full_sync:
        return False

    pdf_file = BytesIO()
    csv_file = StringIO()

    filter_nums = VoipNumFilterData.objects.filter(filter=1)
    filter_nums_lst = list()

    for filter_num in filter_nums:
        filter_nums_lst.append(filter_num.num)

    mediasw_cdr = list(
        VoipCdr.objects.filter(call_dispo="ANSWERED")
        .filter(account=trunk.trunk_name)
        .filter(call_billsec__gte=0)
        .filter(call_start__gte=date_from)
        .filter(call_end__lte=date_to)
        .exclude(num_dst__in=filter_nums_lst)
        .order_by("call_start")
    )

    # check if all rows have zone
    for cdrentry in mediasw_cdr:
        if cdrentry.zone_id is None or cdrentry.billing_units is None:
            return False

    doc = CdrReportDocTemplate(
        subtitle=f"{trunk.customer.name} - {trunk.trunk_name}",
        filename=pdf_file,
    )

    p = ParagraphStyle(name="p", fontSize=8)
    p_center = ParagraphStyle(name="p_center", fontSize=8, alignment=TA_CENTER)

    t_style = [
        ("COLBACKGROUNDS", (0, 0), (-1, 0), [colors.lightgrey, colors.darkgrey]),
        ("BOTTOMPADDING", (0, 0), (-1, -1), 0),
        ("TOPPADDING", (0, 0), (-1, -1), 0),
        ("GRID", (0, 0), (-1, -1), 0.25, colors.gray),
        ("VALIGN", (0, 0), (-1, -1), "TOP"),
    ]

    csv_writer = csv.writer(csv_file, dialect="unix")

    # container for the 'Flowable' objects
    elements = []

    t_data = []

    cdrline = [
        _("Start"),
        _("End"),
        _("Source"),
        _("Destination"),
        _("Duration"),
        _("Units"),
        _("Zone"),
        _("Net Fees"),
    ]
    csv_writer.writerow(cdrline)
    cdrline.remove(_("Zone"))
    t_data.append([Paragraph(value, p_center) for value in cdrline])

    for i, cdrentry in enumerate(mediasw_cdr):
        start = cdrentry.call_answer.strftime("%d.%m.%Y %H:%M:%S")
        end = cdrentry.call_end.strftime("%d.%m.%Y %H:%M:%S")
        num_src = phonenumbers.format_number(
            cdrentry.num_src_obj, phonenumbers.PhoneNumberFormat.INTERNATIONAL
        )
        num_dst = phonenumbers.format_number(
            cdrentry.num_dst_obj, phonenumbers.PhoneNumberFormat.INTERNATIONAL
        )

        duration = str(timedelta(seconds=ceil(cdrentry.call_billsec)))
        units = str(round(cdrentry.billing_units, 2))

        fee = round(cdrentry.billing_units * cdrentry.zone.price, 4)
        fee_with_vat = round(fee * Decimal(1.19), 4)

        if trunk.cdr_report == 1:
            num_dst = num_dst[:-3] + "XXX"

        if is_special_number(cdrentry.num_dst_obj):
            num_dst = str(num_dst)

        csv_writer.writerow(
            [
                start,
                end,
                num_src,
                num_dst,
                duration,
                units,
                cdrentry.zone.name,
                f"€ {fee}",
            ]
        )

        t_data.append(
            [
                Paragraph(start, p),
                Paragraph(end, p),
                Paragraph(num_src, p),
                Paragraph(num_dst, p),
                Paragraph(duration, p_center),
                Paragraph(units, p_center),
                Paragraph(f"€ {fee}", p_center),
            ]
        )
        t_data.append(["", Paragraph(cdrentry.zone.name, p), "", "", "", "", ""])

        t_style.extend(
            [
                ("SPAN", (0, 1 + i * 2), (0, 2 + i * 2)),
                ("SPAN", (1, 2 + i * 2), (-1, 2 + i * 2)),
            ]
        )

    t = Table(
        t_data,
        repeatRows=1,
        colWidths=[None, None, None, None, 2.0 * cm, 2.0 * cm, 3.0 * cm],
    )

    t.setStyle(t_style)

    elements.append(t)
    # write the document to disk
    doc.build(elements)

    cdr_report_data = {
        "pdf": pdf_file.getvalue(),
        "csv": csv_file.getvalue(),
    }

    return cdr_report_data
