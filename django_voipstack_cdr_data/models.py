import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException

from django_voipstack_trunks.django_voipstack_trunks.models import *


class VoipCdr(DirtyFieldsMixin, models.Model):
    id = models.BigAutoField(primary_key=True)
    account = models.CharField(max_length=50, blank=True, null=True)
    trunk = models.ForeignKey(VoipTrunks, models.CASCADE, db_column="trunk", null=True)
    zone = models.ForeignKey(MdatItems, models.CASCADE, null=True)
    billing_units = models.DecimalField(max_digits=19, decimal_places=6, null=True)
    num_src = models.BigIntegerField(null=False, default=0)
    num_dst = models.BigIntegerField(null=False, default=0)
    call_start = models.DateTimeField(null=False)
    call_end = models.DateTimeField(null=False)
    call_answer = models.DateTimeField(blank=True, null=True)
    call_duration = models.DecimalField(
        max_digits=20, decimal_places=10, null=False, default=0
    )
    call_billsec = models.DecimalField(
        max_digits=20, decimal_places=10, null=False, default=0
    )
    call_dispo = models.CharField(max_length=50, blank=True, null=True)
    ama_flags = models.CharField(max_length=50, blank=True, null=True)
    userfield = models.CharField(max_length=50, blank=True, null=True)
    uniqueid = models.CharField(max_length=50, blank=True, null=True)
    linkedid = models.CharField(max_length=50, blank=True, null=True)
    peeraccount = models.CharField(max_length=50, blank=True, null=True)
    cdr_sequence = models.PositiveIntegerField(blank=True, null=True)
    import_flag = models.IntegerField(blank=True, null=True)
    date_added = models.DateTimeField(default=datetime.now)

    @property
    def num_src_obj(self):
        try:
            phonenumbers_src_obj = phonenumbers.parse("+" + str(self.num_src), "DE")
        except NumberParseException:
            phonenumbers_src_obj = phonenumbers.parse(str(self.num_src), "DE")

        return phonenumbers_src_obj

    @property
    def num_dst_obj(self):
        try:
            phonenumbers_dst_obj = phonenumbers.parse("+" + str(self.num_dst), "DE")
        except NumberParseException:
            try:
                phonenumbers_dst_obj = phonenumbers.parse(str(self.num_dst), "DE")
            except NumberParseException:
                phonenumbers_dst_obj = phonenumbers.parse(
                    str(self.num_dst) + "000", "DE"
                )

        return phonenumbers_dst_obj

    class Meta:
        db_table = "voip_cdr"


class VoipCdrSyncState(models.Model):
    name = models.CharField(max_length=200, unique=True)
    sync_state_date = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "voip_cdr_sync_state"
