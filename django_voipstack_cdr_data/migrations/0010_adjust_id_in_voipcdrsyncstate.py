# Generated by Django 3.2.13 on 2022-05-02 10:22

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_cdr_data", "0009_add_date_added_to_voip_cdr"),
    ]

    operations = [
        migrations.AlterField(
            model_name="voipcdrsyncstate",
            name="id",
            field=models.BigAutoField(
                auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
            ),
        ),
    ]
