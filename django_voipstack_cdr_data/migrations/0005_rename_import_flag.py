# Generated by Django 2.1.5 on 2019-02-10 14:06

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_cdr_data", "0004_add_fk_to_trunks_from_cdr"),
    ]

    operations = [
        migrations.RenameField(
            model_name="voipcdr",
            old_name="temp_flag",
            new_name="import_flag",
        ),
    ]
