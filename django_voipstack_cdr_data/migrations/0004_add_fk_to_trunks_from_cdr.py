# Generated by Django 2.1.5 on 2019-02-04 13:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_trunks", "0001_add_trunk_management"),
        ("django_voipstack_cdr_data", "0003_add_temp_flag_field"),
    ]

    operations = [
        migrations.AddField(
            model_name="voipcdr",
            name="trunk",
            field=models.ForeignKey(
                db_column="trunk",
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_trunks.VoipTrunks",
            ),
        ),
    ]
