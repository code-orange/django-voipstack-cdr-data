# Generated by Django 3.2.13 on 2022-05-02 11:56

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_cdr_data", "0012_add_billing_units"),
    ]

    operations = [
        migrations.AlterField(
            model_name="voipcdr",
            name="billing_units",
            field=models.DecimalField(decimal_places=6, max_digits=19, null=True),
        ),
    ]
